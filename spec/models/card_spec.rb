require 'rails_helper'

RSpec.describe Card, type: :model do
  subject { build(:card) }

  describe 'validation' do
    it { should validate_presence_of(:title) }
    it { should validate_length_of(:title) }
    it { should validate_presence_of(:image) }
  end

  describe 'association' do
    it { should have_many(:card_games) }
    it { should have_many(:games).through(:card_games) }
  end
end
