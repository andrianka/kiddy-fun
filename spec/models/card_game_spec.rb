require 'rails_helper'

RSpec.describe Game, type: :model do
  subject { build(:card_game) }

  describe 'validation' do
    it { should validate_presence_of(:card) }
    it { should validate_presence_of(:game) }
    it { should validate_presence_of(:status) }
  end

  describe 'association' do
    it { should belong_to(:game) }
    it { should belong_to(:card) }
  end
end
