require 'rails_helper'

RSpec.describe Game, type: :model do
  subject { build(:game) }

  describe 'validation' do
    it { should validate_presence_of(:status) }
    it { should validate_numericality_of(:number_of_cards) }
    it { should validate_numericality_of(:number_of_cards).only_integer }
    it { should validate_numericality_of(:number_of_cards).even }
    it { should validate_numericality_of(:number_of_cards).is_less_than_or_equal_to(20) }
  end

  describe 'association' do
    it { should have_many(:card_games) }
    it { should have_many(:cards).through(:card_games) }
    it { should belong_to(:user) }
  end

  describe 'attributes' do
  end
end
