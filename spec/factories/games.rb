FactoryBot.define do
  factory :game do
    score { 1 }
    status { Game.statuses[:new] }
    user
  end
end
