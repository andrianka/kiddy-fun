require 'faker'
FactoryBot.define do
  factory :card do
    title { Faker::Lorem.characters(number: 10) }
  end
end
