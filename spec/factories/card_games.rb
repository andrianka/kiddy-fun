FactoryBot.define do
  factory :card_game do
    card
    game
    status { CardGame.statuses[:new] }
  end
end
