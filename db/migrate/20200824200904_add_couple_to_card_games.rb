class AddCoupleToCardGames < ActiveRecord::Migration[6.0]
  def change
    add_column :card_games, :couple, :boolean, default: false
  end
end
