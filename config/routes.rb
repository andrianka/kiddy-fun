Rails.application.routes.draw do
  resources :games
  devise_for :users
  root to: 'cards#index'
  resources :cards
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
