class CardGame < ApplicationRecord
  attribute :status, default: -> { CardGame.statuses[:new] }

  enum status: {
    new: 'New',
    open: 'Open',
    done: 'Done'
    },  _suffix: true

  belongs_to :card
  belongs_to :game

  validates_presence_of :status, :card, :game
  validates :status, inclusion: { in: CardGame.statuses }

  scope :first_card, ->(id) { where(card_id: id, couple: false) }
  scope :second_card, ->(id) { where(card_id: id, couple: true) }

end
