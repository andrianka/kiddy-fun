class Game < ApplicationRecord
  attribute :status, default: -> { Game.statuses[:new] }

  attr_accessor :number_of_cards
  enum status: {
    new: 'New',
    pending: 'Pending',
    done: 'Done'
    },  _suffix: true

  has_many :card_games, dependent: :destroy
  has_many :cards, through: :card_games
  belongs_to :user

  validates_associated :card_games
  validates_numericality_of :number_of_cards,
                            {even: true, only_integer: true, less_than_or_equal_to: 20}

  validates_presence_of :status, :number_of_cards
  validates :status, inclusion: { in: Game.statuses }

end
