class Card < ApplicationRecord
  has_many :card_games, dependent: :destroy
  has_many :games, through: :card_games
  has_one_attached :image

  validates_presence_of :title, :image
  validates_length_of :title, { maximum: 50 }

end
