class CardService::Create

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(card)
    @card = card
  end

  def execute
    return OpenStruct.new(success?: false, card: nil, errors: @card.errors) unless @card.save
    OpenStruct.new(success?: true, card: @card, errors: nil)
  end

end
