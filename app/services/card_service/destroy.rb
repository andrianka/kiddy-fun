class CardService::Destroy

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(card)
    @card = card
  end

  def execute
    return OpenStruct.new(success?: false, card: nil, errors: @card.errors) unless @card.destroy
    delete_image
    OpenStruct.new(success?: true, card: @card, errors: nil)
  end

  private

  def delete_image
    @card.image.purge
  end
end
