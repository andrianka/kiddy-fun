class GameService::Create

  def self.call(*args, &block)
    new(*args, &block).execute
  end

  def initialize(user, params)
    @current_user = user
    @params = params
    @number_of_cards = @params[:number_of_cards].to_i
  end

  def execute
    create_game
    return OpenStruct.new(success?: false, game: nil, errors: @game.errors) unless @game.save

    create_cards_to_game
    OpenStruct.new(success?: true, game: @game, errors: nil)
  end

  private

  def create_game
    @game = @current_user.games.new(@params)
  end

  def create_cards_to_game
    number = get_random_cards(@number_of_cards)
    @game.card_ids = number + number.shuffle!
    @game.card_games.limit(@number_of_cards / 2).update_all(couple: true)
  end

  def get_random_cards(number)
    Card.ids.sample(number / 2)
  end
end
