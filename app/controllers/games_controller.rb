class GamesController < ApplicationController
  before_action :authenticate_user!

  def index
    @games = current_user.games
  end

  def new
    @game = Game.new
  end

  def create
    result = GameService::Create.call(current_user, game_params) #.call(user, params)
    if result.success?
      flash[:success] = "Good luck!"
      redirect_to game_path(result.game)
    else
      flash[:error] = result.errors.full_messages
      render :new
    end
  end

  private

  def set_game
    @game = current_user.games.find(params[:id])
  end

  def game_params
    params.fetch(:game, {}).permit(:number_of_cards)
  end
end
