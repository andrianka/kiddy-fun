class CardsController < ApplicationController
  before_action :set_card, only: [:show, :update, :destroy]

  def index
    @cards = Card.all
  end

  def new
    @card = Card.new
  end

  def create
    result = CardService::Create.call(Card.new(card_params))
    if result.success?
      flash[:success] = "You created new card."
      redirect_to cards_path
    else
      flash[:error] = result.errors.full_messages
      render :new
    end
  end

  def update
    if @card.update_attributes(card_params)
      flash[:success] = "You updated your card."
      redirect_to card_path(@card)
    else
      render :show, error: @card.errors
    end
  end

  def destroy
    result = CardService::Destroy.call(@card)
    if result.success?
      flash[:success] = "You deleted your card."
    else
      flash[:error] = @card.errors
    end
    redirect_to cards_path
  end

  private
  def set_card
    @card = Card.find(params[:id])
  end

  def card_params
    params.fetch(:card, {}).permit(:title, :image)
  end
end
