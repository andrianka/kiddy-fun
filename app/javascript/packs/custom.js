document.addEventListener('turbolinks:load', function(event){
  formFileInput = document.querySelector('.form-file-input')
  if (formFileInput !== null) {
    formFileInput.addEventListener('change',function(e){
      var fileName = document.getElementById("cardFile").files[0].name;
      var nextSibling = e.target.nextElementSibling
      nextSibling.innerText = fileName
    })
  }

  // flash messages remove after 3 sec
  flash_alert = document.getElementById("myAlert");
  if (flash_alert !== null) {
    setTimeout(function(){
      flash_alert.remove('show');
    }, 3000);
  }
});
