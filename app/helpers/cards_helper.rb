module CardsHelper

  def set_file_name(card)
    card.new_record? ? "Choose file..." : card.image.blob.filename
  end
end
